﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CalculateRadius
{
    public partial class Form1 : Form
    {
        private Int32 H, H1, L, R;
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (H * H1 * L * R == 0) { return; }
            double hh = (double)Math.Max(H, H1) - (double)Math.Min(H, H1);
            double cc = Math.Sqrt(hh * hh + (double)L * (double)L) / 2;
            height.Text = ((double)R - Math.Sqrt(R * R - cc*cc)).ToString();
        }

        private void R_TextBox_TextChanged(object sender, EventArgs e)
        {
            if (R_TextBox.Text == "") { return; }
            if (R_TextBox.Text == "0") { return; }
            try
            {
                R = Convert.ToInt32(R_TextBox.Text);
            }
            catch { }
        }



        private void H1_TextBox_TextChanged(object sender, EventArgs e)
        {
            if (H1_TextBox.Text == "") { return; }
            if (H1_TextBox.Text == "0") { return; }
            try
            {
                H1 = Convert.ToInt32(H1_TextBox.Text);
            }
            catch { }
        }

        private void H_TextBox_TextChanged(object sender, EventArgs e)
        {
            if (H_TextBox.Text == "") { return; }
            if (H_TextBox.Text == "0") { return; }
            try
            {
                H = Convert.ToInt32(H_TextBox.Text);
            }
            catch { }
        }
        private void L_TextBox_TextChanged(object sender, EventArgs e)
        {
            if (L_TextBox.Text == "") { return; }
            if (L_TextBox.Text == "0") { return; }
            try
            {
                L = Convert.ToInt32(L_TextBox.Text);
            }
            catch { }
        }
    }
}
